using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovPez : MonoBehaviour
{ public float PlayerSpeed;
    public Sprite PezNormal;
    public Sprite PezMuerto;
    public SpriteRenderer spriteRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)|| (Input.GetKey(KeyCode.LeftArrow)))
        {
            transform.Translate(PlayerSpeed * Vector2.left);
            if (transform.position.x < -10)
            {
                transform.position = new Vector3(-10, transform.position.y, transform.position.z);
            }
        }
        if (Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)))
        {
            transform.Translate(PlayerSpeed * Vector2.right);
            if (transform.position.x > 10)
            {
                transform.position = new Vector3(10, transform.position.y, transform.position.z);
            }
        }
        if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)))
        {
            transform.Translate(PlayerSpeed * Vector2.up);
            if (transform.position.y > 4.9)
            {
                transform.position = new Vector3(transform.position.x, 4.9f, transform.position.z);
            }
        }
        if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)))
        {
            transform.Translate(PlayerSpeed * Vector2.down);
            if (transform.position.y < -4.9)
            {
                transform.position = new Vector3(transform.position.x, -4.9f, transform.position.z);
            }
        }

    }
    private void OnEnable()
    {
        GetComponent<Collider2D>().enabled = true;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Finish"))
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Additive);
            spriteRenderer.sprite = PezMuerto;

        }
    }
   
}
